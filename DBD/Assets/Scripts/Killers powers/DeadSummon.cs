using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadSummon : MonoBehaviour
{
    public GameObject zombie;
    private GameObject newZombies;
    public Vector3 futurePosition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Dead_Summon()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            futurePosition = Vector3.forward + transform.position;
            newZombies = Instantiate (zombie, futurePosition, Quaternion.identity);
        }
    }

}
