using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeControl : MonoBehaviour
{
    public GameObject[] generadores;
    public Transform spawnpoints;
    public AudioSource timeTravel;
    //public Transform spawnManager;

    void Start()
    {
        generadores = GameObject.FindGameObjectsWithTag("Generators");
        spawnpoints = GameObject.FindGameObjectWithTag("Spawn").transform;
        
        //spawnpoints = new Transform[transform.childCount];
        //for (int i = 0; i < transform.childCount; i++)
        //{
        //    spawnpoints[i] = transform.GetChild(i);
        //}
    }

    // Update is called once per frame
    void Update()
    {
        ReplaceGenerators();
    }

   public void ReplaceGenerators()
   {
        if (Input.GetKeyDown(KeyCode.E))
        {
            for (int i = 0; i < generadores.Length; i++)
            {
                timeTravel.Play();
                int newindex = Random.Range(0, spawnpoints.childCount);
                while (spawnpoints.GetChild(newindex).childCount>0)
                {
                    newindex = Random.Range(0, spawnpoints.childCount);
                }
                generadores[i].transform.position = spawnpoints.GetChild(newindex).position;
                generadores[i].transform.parent = spawnpoints.GetChild(newindex);
            }
        }

   }

}
