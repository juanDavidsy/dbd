using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection : MonoBehaviour
{
    public LayerMask lm;
    public float offset;
    public Slenderman_Power power;

    private void Awake()
    {
        GetComponent<CapsuleCollider>().enabled = true;
    }
    void Start()
    {
        power = GameObject.FindGameObjectWithTag("Killer").GetComponent<Slenderman_Power>();
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 150, lm))
        {
            offset += GetComponent<CapsuleCollider>().height / 2;
            transform.position = new Vector3(transform.position.x, transform.position.y - hit.distance + offset, transform.position.z);
            power.futurePosition = transform.position;
        }


    }

    private void Update()
    {
        StartCoroutine(Esperar());
    }
    private void OnCollisionEnter(Collision collision)
    {
        power.isTeleporting = 1;
        Destroy(gameObject);
    }


    IEnumerator Esperar()
    {
        //yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0.5f);
        GetComponent<CapsuleCollider>().enabled = false;
        power.isTeleporting = 0;
    }

}
