using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slenderman_Power : MonoBehaviour
{
    [SerializeField] private GameObject obj;
    public GameObject player;
    [SerializeField] private float radius;
    [SerializeField] public int isTeleporting;
    private GameObject newCilindro;
    public Vector3 futurePosition;
    public AudioSource teleport;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetButtonDown("Power")) 
        if (Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(CTeleport());
        }

    }
    IEnumerator CTeleport()
    {
        isTeleporting = 2;
        while (isTeleporting >= 1)
        {
            float posicionEnX = Random.Range(-radius, radius);
            float posicionEnZ = Mathf.Sign(Random.Range(-1, 1)) * Mathf.Sqrt(Mathf.Pow(radius, 2) - Mathf.Pow(posicionEnX, 2));
            futurePosition = new Vector3(posicionEnX, 0, posicionEnZ) + player.transform.position;
            newCilindro = Instantiate(obj, futurePosition, Quaternion.identity);
            isTeleporting = 2;
            while (isTeleporting >= 2)
            {
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();
        }
        transform.position = futurePosition;
        teleport.Play();
        Destroy(newCilindro.gameObject);
    }

}
